<?php
/**
 * @file
 * Defines the redmine_query class.
 */

/**
 * Extends rest_api_query class to for use with Redmine's REST API.
 *
 * @see http://www.redmine.org/projects/redmine/wiki/Rest_api
 */
class redmine_query extends rest_api_query {
  protected $rest_api_schema = 'redmine';

  /**
   * Constructor requires a valid API key and access point URL to initialize.
   *
   * These can be set with Drupal variables instead.  E.g. In settings.php:
   * @code
   *  $conf['redmine_api_key'] = 'VALUE';
   *  $conf['redmine_access_point'] = 'VALUE';
   * @code
   *
   * @param $api_key String
   *    The API key to authenticate with Redmine via the API.  Defaults to
   *    variable_get('redmine_api_key')
   * @param $access_point String
   *    The Base URL to access the Redmine instance.  Defaults to 
   *    variable_get('redmine_access_point')
   */
  function __construct($api_access_key = NULL, $access_point = NULL) {
    $settings = array(
      'api_key' => $api_access_key,
      'access_point' => $access_point,
    );
    parent::__construct($settings);
  }

  /**
   * Fetches ALL the Redmine items, despite the limits of Redmine's pager.
   *
   * @see redmine_api_fetch_all()
   */
  function fetch_all() {
    // Overwrite or set the "limit" parameter.
    $this->parameters['limit'] = REDMINE_API_LIMIT_MAX;

    // Initialize the offset and make it easily accessible.
    $this->parameters['offset'] = 0;
    $offset = &$this->parameters['offset'];

    // Get the plural form of the resource type.
    $type = get_rest_api_type('redmine', $this->resource);

    // Use offset as the index to control iteration.
    for ($return = array(); $offset <= $this->result->total_count; $offset += REDMINE_API_LIMIT_MAX) {
      // Execute the query and merge in the result set.
      if ($this->execute()->success && is_array($this->result->{$type})) {
        $return = array_merge($return, $this->result->{$type});
      }
      else {
        // This case indicates an error, such as 404 or 403 or an error in the
        // API and/or iteration logic.
      }
    }

    return $return;
  }
}


/**
 * Extends redmine_query class to set the API Access Key from user profiles.
 *
 * @todo Postponed: Support authentication by username & password too.
 */
class redmine_user_access_query extends redmine_query {
  // Make the api_key overridable.
  public $api_key;

  /**
   * Constructor requires a valid API key and access point URL to initialize.
   *
   * These can be set with Drupal variables instead.  E.g. In settings.php:
   * @code
   *  $conf['redmine_api_key'] = 'VALUE';
   *  $conf['redmine_access_point'] = 'VALUE';
   * @code
   *
   * @param $user Mixed
   *    Depending on the type;
   *    - NULL (default):  The current user's API access key
   *    - Integer:  User ID of the user whose API access key should be used.
   *    - Object:  Drupal user object of the user whose API access key to use.
   *    - String:  API access key
   * @param $access_point String
   *    The Base URL to access the Redmine instance.  Defaults to
   *    variable_get('redmine_access_point')
   */
  function __construct($user = NULL, $access_point = NULL) {
    $access_key = get_user_access_key('redmine_api_access_key', $user);

    // Hand off the rest to redmine_query class.
    parent::__construct($access_key, $access_point);
  }
}
